package com.bertolo1988.martingal_simulator;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.bertolo1988.martingal_simulator.ai.BetInAllPlayer;
import com.bertolo1988.martingal_simulator.ai.BetOnlyOnceDummyPlayer;
import com.bertolo1988.martingal_simulator.ai.BetOnlyRedPlayer;
import com.bertolo1988.martingal_simulator.ai.Player;
import com.bertolo1988.martingal_simulator.bet_board.Bet;
import com.bertolo1988.martingal_simulator.bet_board.BetBoard;
import com.bertolo1988.martingal_simulator.bet_board.BetData;

import junit.framework.TestCase;

public class InfraestructureTest extends TestCase {

	@Test
	public void testRouletteBasicSpin() {
		Roulette r = new Roulette(0);
		int a = r.spin();
		int b = r.spin();
		int c = r.spin();
		assertTrue(r.getHistory().get(0) == a);
		assertTrue(r.getHistory().get(1) == b);
		assertTrue(r.getHistory().get(2) == c);
	}

	@Test
	public void testBet() {
		BetData s1 = new BetData(25, new int[] { 0 });
		BetData s2 = new BetData(25, new int[] { 1 });
		BetData s3 = new BetData(25, BetBoard.getFirstHalf());
		ArrayList<BetData> bets = new ArrayList<BetData>();
		bets.add(s1);
		bets.add(s2);
		bets.add(s3);
		Bet bet = new Bet(0, bets);
		Assert.assertTrue(bet.getTotalBetAmount() == 75);
		Assert.assertTrue(bet.retrieveTotalWinnings(0) == (25 * 36));
		Assert.assertTrue(bet.retrieveTotalWinnings(5) == 50);
	}

	@Test
	public void testBetOnlyOncePlayer() {
		Player player = new BetOnlyOnceDummyPlayer();
		Game game = new Game(player, 15, 25, false);
		game.play();
		Assert.assertTrue(game.getDraws() == 1);
	}

	@Test
	public void testOnlyRedDummyPlayer() {
		Player player = new BetOnlyRedPlayer();
		Game game = new Game(player, 15, 25, false);
		game.play();
		Assert.assertTrue(game != null);
	}

	@Test
	public void testBetAll() {
		Player player = new BetInAllPlayer();
		Game game = new Game(player, 15, 25, false);
		game.play();
		Assert.assertTrue(game != null);
	}

	@Test
	public void testRouletteHistory() {
		Roulette r = new Roulette(0);
		int firstSpin = r.spin();
		int secondSpin = r.spin();
		Assert.assertTrue(r.getLastSpin() == secondSpin);
		Assert.assertTrue(r.getOldestSpin() == firstSpin);

		for (int i = 2; i < r.getHistorySize(); i++) {
			r.spin();
		}
		Assert.assertTrue(r.getOldestSpin() == firstSpin);
		r.spin();
		Assert.assertTrue(r.getOldestSpin() == secondSpin);
	}

}
