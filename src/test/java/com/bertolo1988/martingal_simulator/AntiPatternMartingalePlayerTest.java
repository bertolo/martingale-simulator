package com.bertolo1988.martingal_simulator;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.bertolo1988.martingal_simulator.ai.AntiPatternMartingalePlayer;
import com.bertolo1988.martingal_simulator.bet_board.BetBoard;
import com.bertolo1988.martingal_simulator.bet_board.BetType;

public class AntiPatternMartingalePlayerTest {

	private static int MAX_DRAWS = 150;
	private static int STARTING_MONEY = 25;
	private static int OBJECTIVE_MONEY = 50;
	private static int STARTING_BET = 1;
	private static int ROULETTES = 10;

	private static int ANALYZED_DRAWS = 7;

	@Test
	public void testRoulettePatternGetter() {
		AntiPatternMartingalePlayer player = new AntiPatternMartingalePlayer(MAX_DRAWS, OBJECTIVE_MONEY, STARTING_BET,
				ANALYZED_DRAWS);
		List<Integer> lastDrawsBlack = builderLastDraws(BetBoard.getBlackHouses());
		List<Integer> lastDrawsSecondHalf = builderLastDraws(BetBoard.getSecondHalf());
		BetType blackBet = player.getRoulettePattern(lastDrawsBlack);
		BetType secondHalfBet = player.getRoulettePattern(lastDrawsSecondHalf);
		Assert.assertTrue(blackBet.equals(BetType.BLACK));
		Assert.assertTrue(secondHalfBet.equals(BetType.SECOND_HALF));

	}

	private List<Integer> builderLastDraws(int[] houses) {
		List<Integer> lastDraws = new LinkedList<Integer>();
		for (int house : houses) {
			lastDraws.add(house);
		}
		return lastDraws;
	}

	@Test
	public void basicTestAntiPatternPlayer() {
		AntiPatternMartingalePlayer player = new AntiPatternMartingalePlayer(MAX_DRAWS, OBJECTIVE_MONEY, STARTING_BET,
				ANALYZED_DRAWS);
		Game game = new Game(player, ROULETTES, STARTING_MONEY, true);
		int gameResult = game.play();
		Assert.assertTrue(gameResult >= 0);
	}

	@Test
	public void testMoneyObjective() {
		AntiPatternMartingalePlayer player = new AntiPatternMartingalePlayer(MAX_DRAWS, STARTING_MONEY, STARTING_MONEY,
				ANALYZED_DRAWS);
		Game game = new Game(player, ROULETTES, STARTING_MONEY, false);
		int gameResult = game.play();
		Assert.assertTrue(gameResult == 25 && game.getDraws() == 0);
	}

	@Test
	public void testMaxDraws() {
		AntiPatternMartingalePlayer player = new AntiPatternMartingalePlayer(1, OBJECTIVE_MONEY, STARTING_BET,
				ANALYZED_DRAWS);
		Game game = new Game(player, ROULETTES, STARTING_MONEY, false);
		int gameResult = game.play();
		Assert.assertTrue(game.getDraws() == 1 && gameResult >= STARTING_MONEY - 1);
	}
}
