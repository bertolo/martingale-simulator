package com.bertolo1988.martingal_simulator;

import org.junit.Assert;
import org.junit.Test;

import com.bertolo1988.martingal_simulator.ai.BlindMartingalePlayer;
import com.bertolo1988.martingal_simulator.bet_board.BetBoard;

public class BlindMartingalePlayerTest {

	private static int MAX_DRAWS = 100;
	private static int STARTING_MONEY = 25;
	private static int OBJECTIVE_MONEY = 50;
	private static int STARTING_BET = 1;
	private static int ROULETTES = 10;

	@Test
	public void basicTestAntiPatternPlayer() {
		BlindMartingalePlayer player = new BlindMartingalePlayer(MAX_DRAWS, OBJECTIVE_MONEY, STARTING_BET,
				BetBoard.getEvens());
		Game game = new Game(player, ROULETTES, STARTING_MONEY, true);
		int gameResult = game.play();
		Assert.assertTrue(gameResult >= 0);
	}

}
