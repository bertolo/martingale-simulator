package com.bertolo1988.martingal_simulator;

import org.junit.Assert;
import org.junit.Test;

import com.bertolo1988.martingal_simulator.ai.AntiPatternMartingalePlayer;
import com.bertolo1988.martingal_simulator.ai.BetOnlyRedPlayer;
import com.bertolo1988.martingal_simulator.ai.BlindMartingalePlayer;
import com.bertolo1988.martingal_simulator.analyzer.InteligenceAnalyzer;
import com.bertolo1988.martingal_simulator.bet_board.BetBoard;

public class InteligenceAnalyzerTest {

	private static int MAX_DRAWS = 100;
	private static int STARTING_MONEY = 25;
	private static int OBJECTIVE_MONEY = 50;
	private static int STARTING_BET = 1;
	private static int ROULETTES = 10;

	private int ANALYZED_DRAWS = 4;
	private int SESSIONS = 150000;

	@Test
	public void testInteligenceAnalyzerBlindMartingale() {
		BlindMartingalePlayer player = new BlindMartingalePlayer(MAX_DRAWS, OBJECTIVE_MONEY, STARTING_BET,
				BetBoard.getRedHouses());
		InteligenceAnalyzer ia = new InteligenceAnalyzer(player, SESSIONS, OBJECTIVE_MONEY, ROULETTES, STARTING_MONEY,
				false);
		System.out.println("BlindMartingale\n" + ia.toString() + "\n\n");
		Assert.assertTrue(ia != null);
	}

	@Test
	public void testInteligenceAnalyzerBetOnlyRed() {
		BetOnlyRedPlayer player = new BetOnlyRedPlayer();
		InteligenceAnalyzer ia = new InteligenceAnalyzer(player, SESSIONS, OBJECTIVE_MONEY, ROULETTES, STARTING_MONEY,
				false);
		System.out.println("BetOnlyRed\n" + ia.toString() + "\n\n");
		Assert.assertTrue(ia != null);
	}

	@Test
	public void testInteligenceAnalyzerAntiPattern() {
		AntiPatternMartingalePlayer player = new AntiPatternMartingalePlayer(MAX_DRAWS, OBJECTIVE_MONEY, STARTING_BET,
				ANALYZED_DRAWS);
		InteligenceAnalyzer ia = new InteligenceAnalyzer(player, SESSIONS, OBJECTIVE_MONEY, ROULETTES, STARTING_MONEY,
				false);
		System.out.println("AntiPattern\n" + ia.toString() + "\n\n");
		Assert.assertTrue(ia != null);
	}

}
