package com.bertolo1988.martingal_simulator;

import java.util.LinkedList;
import java.util.Random;

public class Roulette {

	private int id;
	private LinkedList<Integer> history;
	private Random randomSeed;
	private static int HISTORY_SIZE = 10;
	private static int HOUSE_NUMBER = 36;

	public Roulette(int id) {
		super();
		this.id = id;
		this.randomSeed = new Random();
		this.history = new LinkedList<Integer>();
	}

	public int getId() {
		return id;
	}

	public int getHistorySize() {
		return HISTORY_SIZE;
	}

	public LinkedList<Integer> getHistory() {
		return history;
	}

	public int spin() {
		int spinResult = randomSeed.nextInt(HOUSE_NUMBER + 1);
		updateHistory(spinResult);
		return spinResult;
	}

	public int getOldestSpin() {
		return history.getFirst();
	}

	public int getLastSpin() {
		return history.getLast();
	}

	private void updateHistory(int house) {
		if (history.size() == HISTORY_SIZE) {
			history.remove(0);
		}
		history.add(house);
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("RouletteId ").append(id).append(" - \t");
		for (Integer h : history) {
			result.append(h + "\t");
		}
		return result.toString();
	}
}
