package com.bertolo1988.martingal_simulator.ai;

import java.util.ArrayList;

import com.bertolo1988.martingal_simulator.Roulette;
import com.bertolo1988.martingal_simulator.bet_board.Bet;
import com.bertolo1988.martingal_simulator.bet_board.BetBoard;
import com.bertolo1988.martingal_simulator.bet_board.BetData;

public class BetOnlyRedPlayer extends Player {

	private static int BET_TIMES = 1;
	private int times;

	public BetOnlyRedPlayer() {
		super();
		this.times = BET_TIMES;
	}

	public Bet play(ArrayList<Roulette> roulettes, int netWorth) {
		times--;
		BetData bet = new BetData(netWorth, BetBoard.getRedHouses());
		return new Bet(roulettes.get(0).getId(), bet);
	}

	public boolean leftGame(int netWorth) {
		return times == 0;
	}

	public void reset() {
		this.times = BET_TIMES;
	}

}
