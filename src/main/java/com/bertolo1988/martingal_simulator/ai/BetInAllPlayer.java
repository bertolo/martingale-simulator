package com.bertolo1988.martingal_simulator.ai;

import java.util.ArrayList;

import com.bertolo1988.martingal_simulator.Roulette;
import com.bertolo1988.martingal_simulator.bet_board.Bet;
import com.bertolo1988.martingal_simulator.bet_board.BetBoard;
import com.bertolo1988.martingal_simulator.bet_board.BetData;

public class BetInAllPlayer extends Player {

	private boolean leftGame;

	public BetInAllPlayer() {
		super();
		reset();
	}

	public Bet play(ArrayList<Roulette> roulettes, int netWorth) {
		leftGame = true;
		int netWorthAux = netWorth;
		ArrayList<BetData> bets = new ArrayList<BetData>();
		for (int house : BetBoard.getAllHouses()) {
			if (netWorthAux > 0) {
				netWorthAux--;
				BetData aux = new BetData(1, new int[] { house });
				bets.add(aux);
			}
		}
		return new Bet(roulettes.get(0).getId(), bets);
	}

	public boolean leftGame(int netWorth) {
		return leftGame;
	}

	public void reset() {
		leftGame = false;
	}

}
