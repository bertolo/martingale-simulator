package com.bertolo1988.martingal_simulator.ai;

import java.util.ArrayList;

import com.bertolo1988.martingal_simulator.Roulette;
import com.bertolo1988.martingal_simulator.bet_board.Bet;

public abstract class Player {

	public abstract Bet play(ArrayList<Roulette> roulettes, int netWorth);

	public abstract boolean leftGame(int netWorth);
	
	public abstract void reset();

}
