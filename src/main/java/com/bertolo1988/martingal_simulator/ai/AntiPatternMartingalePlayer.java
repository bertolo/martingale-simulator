package com.bertolo1988.martingal_simulator.ai;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import com.bertolo1988.martingal_simulator.Roulette;
import com.bertolo1988.martingal_simulator.bet_board.Bet;
import com.bertolo1988.martingal_simulator.bet_board.BetBoard;
import com.bertolo1988.martingal_simulator.bet_board.BetData;
import com.bertolo1988.martingal_simulator.bet_board.BetType;

//this player will try to find a pattern in the roulettes
//and bet against that using the martingale method
//if he has no money to apply x2 of the last bet he will use what is left

public class AntiPatternMartingalePlayer extends Player {

	private int analyzedDraws;
	private int maxDraws;
	private int maxMoney;
	private int actualDraws;
	private int startingBet;
	private int lastNetWorth;
	private LinkedList<Bet> betHistory;

	public AntiPatternMartingalePlayer(int maxDraws, int maxMoney, int startingBet, int analyzedDraws) {
		super();
		this.analyzedDraws = analyzedDraws;
		this.startingBet = startingBet;
		this.maxDraws = maxDraws;
		this.maxMoney = maxMoney;
		reset();
	}

	public void reset() {
		this.actualDraws = 1;
		this.lastNetWorth = 0;
		this.betHistory = new LinkedList<Bet>();
	}

	public Bet play(ArrayList<Roulette> roulettes, int netWorth) {
		if (netWorth < maxMoney) {
			actualDraws++;
			if (netWorth >= lastNetWorth) {
				betHistory.clear();
			}
			lastNetWorth = netWorth;
			if (betHistory.peek() == null) {
				Roulette targetRoulette = chooseRoulette(roulettes);
				if (targetRoulette != null) {
					int[] houses = retrieveBetHouses(targetRoulette);
					BetData betData = new BetData(retrieveBetAmount(), houses);
					Bet bet = new Bet(targetRoulette.getId(), betData);
					betHistory.add(bet);
					return bet;
				}
			} else {
				Bet lastBet = betHistory.getLast();
				int betAmount = retrieveBetAmount() > netWorth ? netWorth : retrieveBetAmount();
				BetData newBetData = new BetData(betAmount, lastBet.getBets().get(0).getHouses());
				Bet bet = new Bet(lastBet.getRouletteId(), newBetData);
				betHistory.add(bet);
				return bet;
			}
			betHistory.clear();
		}
		return null;
	}

	private int retrieveBetAmount() {
		return (int) Math.pow(2, betHistory.size()) * startingBet;
	}

	private int[] retrieveBetHouses(Roulette r) {
		BetType presentPattern = getRoulettePattern(r.getHistory());
		return retrieveAntiPatternBetHouses(presentPattern);
	}

	private Roulette chooseRoulette(ArrayList<Roulette> roulettes) {
		for (Roulette r : roulettes) {
			BetType resultBetType = getRoulettePattern(r.getHistory());
			if (resultBetType != null) {
				return r;
			}
		}
		return null;
	}

	private int[] retrieveAntiPatternBetHouses(BetType resultBetType) {
		switch (resultBetType) {
		case BLACK:
			return BetBoard.getRedHouses();
		case RED:
			return BetBoard.getBlackHouses();
		case FIRST_HALF:
			return BetBoard.getSecondHalf();
		case SECOND_HALF:
			return BetBoard.getFirstHalf();
		case EVEN:
			return BetBoard.getOdds();
		case ODD:
			return BetBoard.getEvens();
		default:
			return new int[] { 0 };
		}
	}

	public BetType getRoulettePattern(List<Integer> lastDraws) {
		if (hasPattern(BetBoard.getBlackHouses(), lastDraws)) {
			return BetType.BLACK;
		} else if (hasPattern(BetBoard.getRedHouses(), lastDraws)) {
			return BetType.RED;
		} else if (hasPattern(BetBoard.getFirstHalf(), lastDraws)) {
			return BetType.FIRST_HALF;
		} else if (hasPattern(BetBoard.getSecondHalf(), lastDraws)) {
			return BetType.SECOND_HALF;
		} else if (hasPattern(BetBoard.getEvens(), lastDraws)) {
			return BetType.EVEN;
		} else if (hasPattern(BetBoard.getOdds(), lastDraws)) {
			return BetType.ODD;
		}
		return null;
	}

	private boolean hasPattern(int[] a, List<Integer> b) {
		for (int i = 1; i <= analyzedDraws; i++) {
			if (!ArrayUtils.contains(a, b.get(b.size() - i))) {
				return false;
			}
		}
		return true;
	}

	public boolean leftGame(int netWorth) {
		return actualDraws > maxDraws || netWorth >= maxMoney;
	}

}
