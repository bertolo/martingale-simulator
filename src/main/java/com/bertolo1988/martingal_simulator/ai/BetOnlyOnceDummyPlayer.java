package com.bertolo1988.martingal_simulator.ai;

import java.util.ArrayList;

import com.bertolo1988.martingal_simulator.Roulette;
import com.bertolo1988.martingal_simulator.bet_board.Bet;
import com.bertolo1988.martingal_simulator.bet_board.BetBoard;
import com.bertolo1988.martingal_simulator.bet_board.BetData;

public class BetOnlyOnceDummyPlayer extends Player {

	private boolean leftGame;

	public BetOnlyOnceDummyPlayer() {
		super();
		reset();
	}

	public Bet play(ArrayList<Roulette> roulettes, int netWorth) {
		leftGame = true;
		BetData bet = new BetData(netWorth, BetBoard.getFirstDozen());
		return new Bet(roulettes.get(0).getId(), bet);
	}

	public boolean leftGame(int netWorth) {
		return leftGame;
	}

	public void reset() {
		leftGame = false;
	}

}
