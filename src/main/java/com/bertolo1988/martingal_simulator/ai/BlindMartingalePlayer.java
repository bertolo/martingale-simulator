package com.bertolo1988.martingal_simulator.ai;

import java.util.ArrayList;
import java.util.LinkedList;

import com.bertolo1988.martingal_simulator.Roulette;
import com.bertolo1988.martingal_simulator.bet_board.Bet;
import com.bertolo1988.martingal_simulator.bet_board.BetData;

public class BlindMartingalePlayer extends Player {

	private int maxDraws;
	private int maxMoney;
	private int actualDraws;
	private int startingBet;
	private int lastNetWorth;
	private LinkedList<Bet> betHistory;
	private int[] houses;

	public BlindMartingalePlayer(int maxDraws, int maxMoney, int startingBet, int[] houses) {
		super();
		reset();
		this.houses = houses;
		this.startingBet = startingBet;
		this.maxDraws = maxDraws;
		this.maxMoney = maxMoney;
	}

	public void reset() {
		this.actualDraws = 1;
		this.lastNetWorth = 0;
		this.betHistory = new LinkedList<Bet>();
	}

	public Bet play(ArrayList<Roulette> roulettes, int netWorth) {
		if (netWorth < maxMoney) {
			actualDraws++;
			if (netWorth >= lastNetWorth) {
				betHistory.clear();
			}
			lastNetWorth = netWorth;
			if (betHistory.peek() == null) {
				BetData betData = new BetData(startingBet, houses);
				Bet bet = new Bet(roulettes.get(0).getId(), betData);
				betHistory.add(bet);
				return bet;

			} else {
				int betAmount = retrieveBetAmount() > netWorth ? netWorth : retrieveBetAmount();
				BetData newBetData = new BetData(betAmount, houses);
				Bet bet = new Bet(roulettes.get(0).getId(), newBetData);
				betHistory.add(bet);
				return bet;
			}
		}
		return null;
	}

	private int retrieveBetAmount() {
		return (int) Math.pow(2, betHistory.size()) * startingBet;
	}

	public boolean leftGame(int netWorth) {
		return actualDraws > maxDraws || netWorth >= maxMoney;
	}

}
