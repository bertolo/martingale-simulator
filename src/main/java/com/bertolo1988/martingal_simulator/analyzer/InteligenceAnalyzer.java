package com.bertolo1988.martingal_simulator.analyzer;

import com.bertolo1988.martingal_simulator.Game;
import com.bertolo1988.martingal_simulator.ai.Player;

public class InteligenceAnalyzer {

	private int reachedMoneyObjective;
	private int wentBroke;
	private int simplyStopped;
	private int gamesPlayed;
	private int profit;
	private int rouletteDraws;
	private int bets;
	private int wonBets;

	public InteligenceAnalyzer(Player player, int sessions, int moneyObjective, int roulettes, int startingMoney,
			boolean showOutput) {
		super();
		this.reachedMoneyObjective = 0;
		this.wentBroke = 0;
		this.simplyStopped = 0;
		this.gamesPlayed = 0;
		this.profit = 0;
		this.rouletteDraws = 0;
		this.bets = 0;
		this.wonBets = 0;

		for (int i = 0; i < sessions; i++) {
			player.reset();
			Game game = new Game(player, roulettes, startingMoney, showOutput);
			int gameResult = game.play();
			rouletteDraws += game.getDraws();
			bets += game.getBets();
			wonBets += game.getWonBets();
			profit = profit + (gameResult - startingMoney);
			gamesPlayed++;
			if (gameResult == 0) {
				wentBroke++;
			} else if (gameResult >= moneyObjective) {
				reachedMoneyObjective++;
			} else {
				simplyStopped++;
			}
		}
	}

	public int getTotalDraws() {
		return rouletteDraws;
	}

	public int getprofit() {
		return profit;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("Total Plays\t\t" + gamesPlayed + "\n");
		result.append("Total Draws\t\t" + rouletteDraws + "\n");
		result.append("Total Bets\t\t" + bets + "\n");
		result.append("Total won bets\t\t" + wonBets + "\n");
		result.append("WonBets/Bets\t\t" + String.format("%.03f", (float) wonBets / bets * 100) + "%\n");
		result.append("Bet per Game\t\t" + String.format("%.02f", (float) bets / rouletteDraws) + "\n");
		result.append("Final Profit\t\t" + profit + "\n\n");
		result.append("Broke\t" + wentBroke + "\t");
		result.append("Ratio per game\t" + String.format("%.02f", (float) wentBroke / gamesPlayed * 100) + " %\n");
		result.append("Success\t" + reachedMoneyObjective + "\t");
		result.append("Ratio per game\t" + String.format("%.02f", (float) reachedMoneyObjective / gamesPlayed * 100)
				+ " %\n");
		result.append("Stopped\t" + simplyStopped + "\t");
		result.append("Ratio per game\t" + String.format("%.02f", (float) simplyStopped / gamesPlayed * 100) + " %\n");
		return result.toString();
	}
}
