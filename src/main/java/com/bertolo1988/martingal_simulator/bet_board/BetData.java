package com.bertolo1988.martingal_simulator.bet_board;

public class BetData {

	private int amount;
	private int[] houses;

	public BetData() {
		super();
		this.amount = 0;
		this.houses = new int[] {};
	}

	public BetData(int amount, int[] houses) {
		super();
		this.amount = amount;
		this.houses = houses;
	}

	public int getAmount() {
		return amount;
	}

	public int[] getHouses() {
		return houses;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setHouses(int[] houses) {
		this.houses = houses;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("Amount: " + amount);
		StringBuilder houses = new StringBuilder();
		houses.append(" [ ");
		for (int house : this.houses) {
			houses.append(house + " ");
		}
		houses.append("]");
		result.append(" Houses: " + houses.toString());
		return result.toString();
	}

	public boolean isWin(int draw) {
		for (int house : houses) {
			if (house == draw) {
				return true;
			}
		}
		return false;
	}

	private float getCasinoOdd() {
		return (houses.length / 37f) * 100;
	}

	public int getWinningMultiplier(int lastSpin) {
		if (isWin(lastSpin)) {
			float percentage = getCasinoOdd();
			if (percentage < 49 && percentage > 48) {
				return 2;
			} else if (percentage < 33 && percentage > 32) {
				return 3;
			} else if (percentage < 17 && percentage > 16) {
				return 6;
			} else if (percentage < 11 && percentage > 10) {
				return 9;
			} else if (percentage < 9 && percentage > 8) {
				return 12;
			} else if (percentage < 6 && percentage > 5) {
				return 18;
			} else if (percentage < 3 && percentage > 2) {
				return 36;
			}
			return 0;
		} else {
			return 0;
		}
	}

}
