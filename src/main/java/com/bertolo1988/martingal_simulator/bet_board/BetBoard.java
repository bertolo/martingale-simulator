package com.bertolo1988.martingal_simulator.bet_board;

public class BetBoard {

	private static final int[] RED_HOUSES = { 1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36 };
	private static final int[] BLACK_HOUSES = { 2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35 };
	private static final int[][] ADJACENCY_TABLE = { { 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36 },
			{ 2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35 }, { 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34 } };
	private static final int[] ODDS = { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35 };
	private static final int[] EVENS = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36 };
	private static final int[] FIRST_HALF = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };
	private static final int[] SECOND_HALF = { 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 };
	private static final int[] ALL_HOUSES = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
			21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 };
	private static final int[] FIRST_DOZEN = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	private static final int[] SECOND_DOZEN = { 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
	private static final int[] THIRD_DOZEN = { 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 };

	public static int[] getRedHouses() {
		return RED_HOUSES;
	}

	public static int[] getBlackHouses() {
		return BLACK_HOUSES;
	}

	public static int[] getOdds() {
		return ODDS;
	}

	public static int[] getEvens() {
		return EVENS;
	}

	public static int[] getFirstHalf() {
		return FIRST_HALF;
	}

	public static int[] getSecondHalf() {
		return SECOND_HALF;
	}

	public static int[] getAllHouses() {
		return ALL_HOUSES;
	}

	public static int[] getFirstDozen() {
		return FIRST_DOZEN;
	}

	public static int[] getSecondDozen() {
		return SECOND_DOZEN;
	}

	public static int[] getThirdDozen() {
		return THIRD_DOZEN;
	}

	public static int[] getFirstLine() {
		return ADJACENCY_TABLE[0];
	}

	public static int[] getSecondLine() {
		return ADJACENCY_TABLE[1];
	}

	public static int[] getThirdLine() {
		return ADJACENCY_TABLE[2];
	}

	public static int[] getColumn(int columnNumber) {
		int[] column = new int[3];
		column[0] = ADJACENCY_TABLE[0][columnNumber];
		column[1] = ADJACENCY_TABLE[1][columnNumber];
		column[2] = ADJACENCY_TABLE[2][columnNumber];
		return column;
	}

	public static boolean isNeighbor(int a, int b) {
		if (a < 4 && b < 4) {
			// 0,1,2,3 are neightbors
			return true;
		}
		int xa = getX(a);
		int ya = getY(a);
		int xb = getX(b);
		int yb = getY(b);
		return Math.hypot(xa - xb, ya - yb) <= 1;
	}

	private static int getX(int a) {
		for (int i = 0; i < ADJACENCY_TABLE.length; i++) {
			for (int j = 0; j < ADJACENCY_TABLE[i].length; j++) {
				if (ADJACENCY_TABLE[i][j] == a) {
					return i;
				}
			}
		}
		return 0;
	}

	private static int getY(int a) {
		for (int i = 0; i < ADJACENCY_TABLE.length; i++) {
			for (int j = 0; j < ADJACENCY_TABLE[i].length; j++) {
				if (ADJACENCY_TABLE[i][j] == a) {
					return j;
				}
			}
		}
		return 0;
	}
}
