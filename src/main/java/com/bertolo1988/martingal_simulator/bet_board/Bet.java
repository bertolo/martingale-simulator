package com.bertolo1988.martingal_simulator.bet_board;

import java.util.ArrayList;

public class Bet {

	private int rouletteId;
	private ArrayList<BetData> bets;

	public Bet(int rouletteId) {
		super();
		this.rouletteId = rouletteId;
		this.bets = new ArrayList<BetData>();
		BetData singleBet = new BetData();
		this.bets.add(singleBet);
	}

	public Bet(int rouletteId, BetData singleBet) {
		super();
		this.rouletteId = rouletteId;
		this.bets = new ArrayList<BetData>();
		this.bets.add(singleBet);
	}

	public Bet(int rouletteId, ArrayList<BetData> bets) {
		super();
		this.rouletteId = rouletteId;
		this.bets = bets;
	}

	public int getTotalBetAmount() {
		int result = 0;
		for (BetData bet : this.bets) {
			result += bet.getAmount();
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (BetData bet : this.bets) {
			result.append(bet.toString() + "\n");
		}
		result.append("On roulette " + rouletteId + "!");
		return result.toString();
	}

	public int retrieveTotalWinnings(int lastSpinDraw) {
		int result = 0;
		for (BetData bet : bets) {
			result = result + bet.getAmount() * bet.getWinningMultiplier(lastSpinDraw);
		}
		return result;
	}

	public int getRouletteId() {
		return rouletteId;
	}

	public ArrayList<BetData> getBets() {
		return bets;
	}

	public void setRouletteId(int rouletteId) {
		this.rouletteId = rouletteId;
	}

	public void setBets(ArrayList<BetData> bets) {
		this.bets = bets;
	}

}
