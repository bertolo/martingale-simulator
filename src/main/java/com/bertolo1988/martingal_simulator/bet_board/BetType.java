package com.bertolo1988.martingal_simulator.bet_board;

public enum BetType {
	LINE, COLUMN, DOZEN, FIRST_HALF, SECOND_HALF, RED, BLACK, ODD, EVEN, PAIR, QUADRUPLE, SINGLE;
}
