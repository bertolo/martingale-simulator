package com.bertolo1988.martingal_simulator;

import java.util.ArrayList;

import com.bertolo1988.martingal_simulator.ai.Player;
import com.bertolo1988.martingal_simulator.bet_board.Bet;

public class Game {

	private Player player;
	private int netWorth;
	private ArrayList<Roulette> roulettes;
	private int draws;
	private int bets;
	private int wonBets;
	private static int INITIAL_DRAWS = 30;
	private boolean showOutput;

	public Game(Player player, int roulettesNumber, int netWorth, boolean showOutput) {
		this.showOutput = showOutput;
		this.roulettes = new ArrayList<Roulette>();
		for (int i = 0; i < roulettesNumber; i++) {
			roulettes.add(new Roulette(i));
		}
		this.player = player;
		this.netWorth = netWorth;
		this.draws = 0;
		this.bets = 0;
		this.wonBets = 0;
		spin(INITIAL_DRAWS);
	}

	public int getDraws() {
		return draws;
	}

	public int getBets() {
		return bets;
	}

	public int getWonBets() {
		return wonBets;
	}

	public int play() {
		while (netWorth > 0 && !player.leftGame(netWorth)) {
			draws++;
			printRoulettesState(roulettes);
			Bet bet = player.play(roulettes, netWorth);
			showBet(bet);
			spin();
			handleBet(bet);
			printThis("NetWorth is " + netWorth + "\n\n");
		}
		return playerIsLeaving();
	}

	private void handleBet(Bet bet) {
		if (isBetValid(bet)) {
			bets++;
			int spin = roulettes.get(bet.getRouletteId()).getLastSpin();
			printThis("Roulette " + bet.getRouletteId() + " Spinned " + spin + "!");
			int winnings = bet.retrieveTotalWinnings(spin);
			netWorth = netWorth + winnings;
			if (winnings > 0) {
				wonBets++;
				printThis("PLAYER WON " + winnings + "!");
			} else {
				printThis("PLAYER LOST " + bet.getTotalBetAmount() + "!");
			}
		} else {
			printThis("No bet!");
		}
	}

	private int playerIsLeaving() {
		if (player.leftGame(netWorth)) {
			printThis(player.getClass().getSimpleName() + " left the game with " + netWorth + " after " + draws
					+ " draws!\n");
			return netWorth;
		} else {
			printThis(player.getClass().getSimpleName() + " went BROKE after " + draws + " draws!\n");
			return 0;
		}
	}

	private boolean isBetValid(Bet bet) {
		return bet != null && bet.getTotalBetAmount() > 0;
	}

	private void showBet(Bet bet) {
		if (bet != null) {
			netWorth = netWorth - bet.getTotalBetAmount();
			printThis(bet.toString());
		}
	}

	private void spin() {
		spin(1);
	}

	private void printRoulettesState(ArrayList<Roulette> roulettes) {
		printThis("Draw " + draws);
		for (Roulette r : roulettes) {
			printThis(r.toString());
		}
	}

	private void spin(int rounds) {
		for (int i = 0; i < rounds; i++) {
			for (Roulette r : roulettes) {
				r.spin();
			}
		}
	}

	private void printThis(String str) {
		if (showOutput) {
			System.out.println(str);
		}
	}

}
