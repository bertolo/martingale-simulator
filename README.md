# martingale-simulator

This Java project consists in an environment to test roulette players intelligence.

## FAQ

If you are using eclipse and get a Maven configuration problem such as:

`org.codehaus.plexus.archiver.jar.Manifest.write(java.io.PrintWriter)	
pom.xml	/martingal-simulator	line 1	`

Update your Maven Archiver using Help -> Install New Software on top using the following link:
https://otto.takari.io/content/sites/m2e.extras/m2eclipse-mavenarchiver/0.17.2/N/LATEST/

## Conclusion

Using the class InteligenceAnalyzerTest with the following configuration for the BetOnlyRed, BlindMartingale and AntiPatternMartingale players.

```
#!java
	private static int MAX_DRAWS = 100;
	private static int STARTING_MONEY = 25;
	private static int OBJECTIVE_MONEY = 50;
	private static int STARTING_BET = 1;
	private static int ROULETTES = 10;

	private int ANALYZED_DRAWS = 4;
	private int SESSIONS = 100000;
```

We reached the irrefutable conclusion that martingale method does not work even if you try to bet after roulette patterns.

**The longer the game, the slimmer the chance.**

Here are some of the values that we got:

```
#!java
BlindMartingale
Total Plays		150000
Total Draws		5928551
Total Bets		5928551
Total won bets		2884562
WonBets/Bets		48,655%
Bet per Game		1,00
Final Profit		-426077

Broke	82293	Ratio per game	54,86 %
Success	63863	Ratio per game	42,58 %
Stopped	3844	Ratio per game	2,56 %



BetOnlyRed
Total Plays		150000
Total Draws		150000
Total Bets		150000
Total won bets		72768
WonBets/Bets		48,512%
Bet per Game		1,00
Final Profit		-111600

Broke	77232	Ratio per game	51,49 %
Success	72768	Ratio per game	48,51 %
Stopped	0	Ratio per game	0,00 %



AntiPattern
Total Plays		150000
Total Draws		6091652
Total Bets		5920757
Total won bets		2879987
WonBets/Bets		48,642%
Bet per Game		0,97
Final Profit		-413503

Broke	81953	Ratio per game	54,64 %
Success	63751	Ratio per game	42,50 %
Stopped	4296	Ratio per game	2,86 %
```
Here is a sample output produced by a simple game using the AntiPatternMartingalePlayer:
```
#!java
private static int MAX_DRAWS = 5;
private static int STARTING_MONEY = 25;
private static int OBJECTIVE_MONEY = 50;
private static int STARTING_BET = 1;
private static int ROULETTES = 10;
private static int ANALYZED_DRAWS = 6;
Draw 1
RouletteId 0 - 	28	3	29	4	33	14	22	1	24	7	
RouletteId 1 - 	29	10	7	9	14	34	35	12	12	2	
RouletteId 2 - 	30	26	25	30	2	25	28	10	16	13	
RouletteId 3 - 	7	21	27	20	16	25	32	25	13	24	
RouletteId 4 - 	21	36	4	11	0	16	30	35	34	4	
RouletteId 5 - 	9	8	32	23	19	12	18	27	22	10	
RouletteId 6 - 	34	33	7	30	36	10	5	20	6	1	
RouletteId 7 - 	10	3	31	8	7	29	34	29	4	6	
RouletteId 8 - 	33	27	32	1	31	27	32	14	13	13	
RouletteId 9 - 	24	20	9	33	0	22	3	7	7	21	
No bet!
NetWorth is 25


Draw 2
RouletteId 0 - 	3	29	4	33	14	22	1	24	7	7	
RouletteId 1 - 	10	7	9	14	34	35	12	12	2	31	
RouletteId 2 - 	26	25	30	2	25	28	10	16	13	12	
RouletteId 3 - 	21	27	20	16	25	32	25	13	24	22	
RouletteId 4 - 	36	4	11	0	16	30	35	34	4	16	
RouletteId 5 - 	8	32	23	19	12	18	27	22	10	33	
RouletteId 6 - 	33	7	30	36	10	5	20	6	1	22	
RouletteId 7 - 	3	31	8	7	29	34	29	4	6	2	
RouletteId 8 - 	27	32	1	31	27	32	14	13	13	14	
RouletteId 9 - 	20	9	33	0	22	3	7	7	21	18	
No bet!
NetWorth is 25


Draw 3
RouletteId 0 - 	29	4	33	14	22	1	24	7	7	7	
RouletteId 1 - 	7	9	14	34	35	12	12	2	31	5	
RouletteId 2 - 	25	30	2	25	28	10	16	13	12	14	
RouletteId 3 - 	27	20	16	25	32	25	13	24	22	13	
RouletteId 4 - 	4	11	0	16	30	35	34	4	16	23	
RouletteId 5 - 	32	23	19	12	18	27	22	10	33	11	
RouletteId 6 - 	7	30	36	10	5	20	6	1	22	23	
RouletteId 7 - 	31	8	7	29	34	29	4	6	2	18	
RouletteId 8 - 	32	1	31	27	32	14	13	13	14	30	
RouletteId 9 - 	9	33	0	22	3	7	7	21	18	12	
Amount: 1 Houses:  [ 2 4 6 8 10 11 13 15 17 20 22 24 26 28 29 31 33 35 ]
On roulette 9!
Roulette 9 Spinned 8!
PLAYER WON 2!
NetWorth is 26


Draw 4
RouletteId 0 - 	4	33	14	22	1	24	7	7	7	13	
RouletteId 1 - 	9	14	34	35	12	12	2	31	5	12	
RouletteId 2 - 	30	2	25	28	10	16	13	12	14	18	
RouletteId 3 - 	20	16	25	32	25	13	24	22	13	1	
RouletteId 4 - 	11	0	16	30	35	34	4	16	23	20	
RouletteId 5 - 	23	19	12	18	27	22	10	33	11	8	
RouletteId 6 - 	30	36	10	5	20	6	1	22	23	5	
RouletteId 7 - 	8	7	29	34	29	4	6	2	18	34	
RouletteId 8 - 	1	31	27	32	14	13	13	14	30	7	
RouletteId 9 - 	33	0	22	3	7	7	21	18	12	8	
Amount: 1 Houses:  [ 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ]
On roulette 2!
Roulette 2 Spinned 11!
PLAYER LOST 1!
NetWorth is 25


Draw 5
RouletteId 0 - 	33	14	22	1	24	7	7	7	13	2	
RouletteId 1 - 	14	34	35	12	12	2	31	5	12	1	
RouletteId 2 - 	2	25	28	10	16	13	12	14	18	11	
RouletteId 3 - 	16	25	32	25	13	24	22	13	1	25	
RouletteId 4 - 	0	16	30	35	34	4	16	23	20	12	
RouletteId 5 - 	19	12	18	27	22	10	33	11	8	12	
RouletteId 6 - 	36	10	5	20	6	1	22	23	5	4	
RouletteId 7 - 	7	29	34	29	4	6	2	18	34	35	
RouletteId 8 - 	31	27	32	14	13	13	14	30	7	15	
RouletteId 9 - 	0	22	3	7	7	21	18	12	8	7	
Amount: 2 Houses:  [ 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ]
On roulette 2!
Roulette 2 Spinned 35!
PLAYER WON 4!
NetWorth is 27


AntiPatternMartingalePlayer left the game with 27 after 5 draws!
```